$(async function () {
    let enabled = false;

    chrome.storage.local.get([
        "enabled",
        "lastCheck",
        "successed"
    ], function (config) {

        if (enabled = config.enabled) {
            $("#enable").css({ 'background': '#00ff00' });
        } else {
            $("#check").prop("disabled", true);
        }

        if (config.lastCheck)
            if (config.successed) {
                $("#lastchecked").text("最終チェック： " + (new Date(config.lastCheck)).toLocaleString('ja-JP'));
            } else {
                $("#lastchecked").text("最終チェック： " + (new Date(config.lastCheck)).toLocaleString('ja-JP') + "(エラー発生)");
                $("#lastchecked").css("color", "red");

            }

    })

    chrome.runtime.onMessage.addListener(async (request, sender, sendResponse) => {

        switch (request.popupStatus) {
            case "checking": {
                $("#lastchecked").text("読み込み中…");
                break;
            }
            case "successed": {
                $("#lastchecked").text("最終チェック： " + (new Date(await getLocalStorage("lastCheck"))).toLocaleString('ja-JP'));
                $("#lastchecked").css("color", "black");
                break;
            }
            case "failed": {
                $("#lastchecked").text("最終チェック： " + (new Date(await getLocalStorage("lastCheck"))).toLocaleString('ja-JP') + "(エラー発生)");
                $("#lastchecked").css("color", "red");
                break;
            }
        }

        return true;
    });

    const configKeys = getConfigKeys("config");
    const multiscaleRangeValue = multiscaleRange("multiscale-range");
    rangeValueMakeVisible("config");
    getConfigFromStorage(configKeys, multiscaleRangeValue);

    $("#enable").click(function () {
        if (enabled) {
            enabled = false;
            chrome.storage.local.set({ "enabled": false });
            sendMessageToEvent({ process: "disable" });
            $("#enable").css({ 'background': '#979797' });
            $("#check").prop("disabled", true);
        } else {
            enabled = true;
            chrome.storage.local.set({ "enabled": true })
            sendMessageToEvent({ process: "enable" });
            $("#enable").css({ 'background': '#00ff00' });
            $("#check").prop("disabled", false);
        }
    })

    $("#save").click(function () {
        setConfigToStorage(configKeys);
        sendMessageToEvent({ process: "changeSettings" });
    });

    $("#check").click(() => {
        sendMessageToEvent({ process: "forceCheck" });
    })

    $("#keepCheckbox").click(() => {
        sendMessageToEvent({ process: "keep" });
    })

    $(".option").click(function () {
        chrome.runtime.openOptionsPage();
    });

    $('.option-icon').each(function () {
        $(this).hover(
            function () {
                $(this).attr('src', 'png/setting_onmouse.png');
            },
            function () {
                $(this).attr('src', 'png/setting.png');
            }
        );
    });




});