$(async function () {

    getLocalStorage("keepwords").then((keepWords) => {
        if (!keepWords) return;
        for (word of keepWords) {
            $("#keepwords").append('<option value="' + word + '">' + word + '</select>')
        }
        $("#keepwords").attr("size", keepWords.length);
        if (keepWords.length == 0) {
            $("#delete").hide();
            $("#keepwords").hide();
        } else {
            $(".keepwords.empty").hide()
        }
    });

    getLocalStorage("uncheckLog").then((uncheckLog) => {
        if (!uncheckLog) {
            $(".log").hide();
            return;
        }
        for (log of uncheckLog) {
            $("#log").append('<option value="' + log.word + '">' + log.date + " - " + log.word + '</select>')
        }
        $("#log").attr("size", Math.min(uncheckLog.length, 10));
    });


    $("#add").click(() => {
        const addItem = $("#log option:selected");
        getLocalStorage("keepwords").then((keepWords) => {
            if (!keepWords.includes(addItem.val())) {
                keepWords.push(addItem.val());
                chrome.storage.local.set({ keepwords: keepWords });
                $("#keepwords").append('<option value="' + addItem.val() + '">' + addItem.val() + '</select>')

                const listSize = parseInt($("#keepwords").attr("size")) + 1;
                $(".keepwords.empty").hide()
                $("#keepwords").show();
                $("#keepwords").attr("size", listSize);
                $("#delete").show();
            }

        });

    });

    $("#delete").click(() => {
        const deleteItem = $("#keepwords option:selected");
        getLocalStorage("keepwords").then((keepWords) => {
            chrome.storage.local.set({ keepwords: keepWords.filter(item => item != deleteItem.val()) });
        });
        deleteItem.remove();
        const listSize = parseInt($("#keepwords").attr("size")) - 1;
        $("#keepwords").attr("size", listSize);
        if (listSize == 0) {
            $("#delete").hide();
            $("#keepwords").hide();
            $(".keepwords.empty").show()
        }
    });

});