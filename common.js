class Lock {
    constructor() {
        this.locked = false;
        this.waitUnlockPromise = Promise.resolve();
        this.resolver = () => { }
    }

    lock() {
        if (this.locked == true) return false;
        this.locked = true;
        this.waitUnlockPromise = new Promise((resolve, reject) => {
            this.resolver = () => {
                resolve();
            }
        })
    }

    unlock() {
        this.locked = false;
        this.resolver();
    }

    isLocked() {
        return this.locked;
    }

    waitUnlock() {
        return this.waitUnlockPromise;
    }

}

class Timeout {
    constructor(ms) {
        if (!Number.isInteger(ms) | ms < 1) throw "自然数ではありません";
        this.ms = ms;
        this.outerPromise = new Promise((resolve, reject) => {
            this.rejecter = () => {
                reject();
            }
        })

        this.innerPromise = new Promise((resolve, reject) => {
            this.resolver = () => {
                resolve();
            }
            setTimeout(() => {
                reject();
            }, ms)
        });

        this.innerPromise.catch(() => { this.rejecter() })
    }

    reset(ms = this.ms) {
        if (!Number.isInteger(ms) | ms < 1) throw "自然数ではありません";
        this.innerPromise = new Promise((resolve, reject) => {
            this.resolver();
            this.resolver = () => {
                resolve();
            }
            setTimeout(() => {
                reject();
            }, ms)
        });

        this.innerPromise.catch(() => { this.rejecter() })
    }

    promise() {
        return this.outerPromise;
    }

}


function sendMessageToEvent(message) {
    return new Promise((resolve) => {
        chrome.runtime.sendMessage(message, (response) => {
            resolve(response);
        });
    });
}

function sendMessageToActiveTab(message) {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({ active: true, currentWindow: true }, (target) => {
            chrome.tabs.sendMessage(target[0].id, message, (response) => {
                if (response) {
                    resolve(response);
                } else {
                    reject();
                }
            });
        });
    });
}

function sendNotification(title, message) {

    const notificationOptions = {
        type: "basic",
        title: title,
        message: message,
        iconUrl: "png/notificationicon.png"
    };

    sendMessageToEvent({
        process: "notification",
        option: notificationOptions
    })

}

function getConfigKeys(targetClass) {
    const configKeys = [];
    const nodeArray = document.querySelectorAll("." + targetClass);
    nodeArray.forEach((node) => {
        configKeys.push(node.id);
    })
    return configKeys
}

function multiscaleRange(targetClass) {
    if (!targetClass) throw "targetClassが設定されていません";
    const valueData = {};
    const nodeArray = document.querySelectorAll("input." + targetClass + "[type='range']");
    nodeArray.forEach((node) => {
        const variableStep = (() => {
            const maxValue = parseInt(node.getAttribute("max"))
            const variableStep = [parseInt(node.getAttribute("min"))];
            const stepData = node.getAttribute("stepdata").split(",");
            for (data of stepData) {
                const [step, boundary] = data.split(":");
                while (variableStep[variableStep.length - 1] < boundary) {
                    const value = variableStep[variableStep.length - 1] + parseInt(step)
                    if (value >= boundary) {
                        variableStep.push(parseInt(boundary));
                    } else if (value >= maxValue) {
                        variableStep.push(maxValue);
                        return variableStep;
                    } else {
                        variableStep.push(value);
                    }
                }
            }
            return variableStep;
        })();

        node.setAttribute("min", "0");
        node.setAttribute("max", variableStep.length - 1);
        node.removeAttribute("step");

        const currentValueElement = document.querySelector(".current-value[target=" + node.id + "]")
        valueData[currentValueElement.id] = variableStep;
        if (currentValueElement.tagName == "INPUT") {
            const convertedTime = [];
            for (value of variableStep) {
                convertedTime.push(milliSecondConvert(value))
            }
            node.addEventListener('input', (event) => {
                currentValueElement.value = variableStep[event.target.value];
                const convertedTimeElement = document.querySelector(".convertedtime[target=" + node.id + "]")
                convertedTimeElement.innerText = convertedTime[event.target.value];
            });
        } else {
            node.addEventListener('input', (event) => {
                currentValueElement.innerText = variableStep[event.target.value];
            });
        }

    })

    return valueData;
}

function rangeValueMakeVisible(targetClass) {
    const nodeArray = document.querySelectorAll("input." + targetClass + "[type='range']");
    nodeArray.forEach((node) => {
        const currentValueElement = document.querySelector(".current-value[target=" + node.id + "]")
        currentValueElement.innerText = node.value;
        node.addEventListener('input', (event) => {
            currentValueElement.innerText = event.target.value;
        });

    })
}

function milliSecondConvert(milliSecond) {
    if (milliSecond == 0) return "0秒";
    const day = Math.floor(milliSecond / 86400000)
    const hour = Math.floor((milliSecond - day * 86400000) / 3600000);
    const minute = Math.floor((milliSecond - day * 86400000 - hour * 3600000) / 60000)
    const second = (() => {
        if (day > 0 | hour > 0 | minute > 0) {
            return Math.floor((milliSecond - day * 86400000 - hour * 3600000 - minute * 60000) / 1000);
        } else {
            return milliSecond / 1000;
        }
    })();

    return (day > 0 ? day + "日" : "") + (hour > 0 ? hour + "時間" : "") + (minute > 0 ? minute + "分" : "") + (second > 0 ? second + "秒" : "");

}

function getConfigKeys(targetClass) {
    const configKeys = [];
    const nodeArray = document.querySelectorAll("." + targetClass);
    nodeArray.forEach((node) => {
        configKeys.push(node.id);
    })
    return configKeys
}

async function getConfigFromStorage(keys, multiscaleRangeValue = {}) {
    const config = await (async () => {
        return promise = new Promise((resolve) => {
            getLocalStorage(keys).then((result) => {
                resolve(result);
            })
        });
    })();


    if (!config) return;
    for (const key of keys) {
        const inputType = (function () {
            if ($("#" + key).prop("tagName") == "INPUT") { return $("#" + key).attr("type"); }
            else if ($("#" + key).hasClass("current-value")) { return "currentValue"; }
            else { return $("#" + key).prop("tagName"); }
        })();

        if (inputType == "checkbox") {
            if (config[key]) { $("#" + key).prop("checked", true); }
        }
        else if (inputType == "hidden") {
            $("#" + key).val(config[key]);
            const targetId = $("#" + key).attr("target")
            $("#" + targetId).val(multiscaleRangeValue[key].indexOf(parseInt(config[key])));
            document.querySelector("*[target=" + targetId + "]").innerText = milliSecondConvert(config[key]);
        }
        else if (inputType == "currentValue") {
            $("#" + key).text(config[key])
            const targetId = $("#" + key).attr("target")
            $("#" + targetId).val(multiscaleRangeValue[key].indexOf(parseInt(config[key])));
        }
        else if (inputType == "range") {
            $("#" + key).val(config[key]);
            document.querySelector(".current-value[target=" + key + "]").innerText = config[key];
        }
        else {
            $("#" + key).val(config[key])
        }
    }
}

function setConfigToStorage(keys) {
    const configHash = {};
    for (const key of keys) {
        const inputType = (function () {
            if ($("#" + key).prop("tagName") == "INPUT") { return $("#" + key).attr("type"); }
            else if ($("#" + key).hasClass("current-value")) { return "currentValue"; }
            else { return $("#" + key).prop("tagName"); }
        })();
        if (inputType == "checkbox") {
            configHash[key] = $("#" + key).prop("checked");
        }
        else if (inputType == "SELECT") {
            configHash[key] = $("#" + key + " option:selected").val()
        }
        else if (inputType == "currentValue") {
            const checkedAmount = checkAmount($("#" + key).text());
            configHash[key] = checkedAmount;
        }
        else if ($("#" + key).attr("datatype") == "int") {
            const checkedAmount = checkAmount($("#" + key).val());
            configHash[key] = checkedAmount;
            $("#" + key).val(checkedAmount);
        }
        else {
            configHash[key] = $("#" + key).val();
        }
    }

    chrome.storage.local.set(configHash);

}

function getLocalStorage(keys) {

    return promise = new Promise((resolve) => {
        if (Array.isArray(keys)) {
            chrome.storage.local.get(keys, (config) => {
                const configHash = {};
                for (const key of keys) {
                    configHash[key] = config[key];
                }

                resolve(configHash);
            })
        } else {
            chrome.storage.local.get(keys, (config) => {
                resolve(config[keys]);
            })

        }
    })

}

function checkAmount(amount) {
    const checkedAmount = parseInt(amount, 10);
    if (isNaN(checkedAmount)) { return null; }
    else { return checkedAmount >= 0 ? checkedAmount : 0; }
}


function timeoutProcess(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(new Error("timeout(" + ms + "ms)"));
        }, ms)
    });
}

function waitProcess(ms) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(ms + "ms経過");
        }, ms)
    });
}