(function () {

    const timeout = new Timeout(8000);

    const loadingProcess = new Promise(resolve => {
        const target = document.querySelector("body");
        const observer = new MutationObserver(records => {
            if (document.querySelector("section[aria-labelledby='detail-header'] input[type='checkbox']")) {
                resolve();
                observer.disconnect();
                sendMessageToEvent({ status: "loaded" });
            }
            timeout.reset();
        });

        observer.observe(target, {
            childList: true,
            subtree: true,
            characterData: false,
            attributes: false
        });

    });

    Promise.race([loadingProcess, timeout.promise()]).catch(() => {
        sendMessageToEvent({ status: "timeout" });
    });


    chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
        switch (request.process) {
            case "uncheck": {
                const checkBox = document.evaluate("//section[@aria-labelledby='detail-header']//input[@type='checkbox']", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
                const interestWords = document.evaluate("//section[@aria-labelledby='detail-header']//span[../..//input[@type='checkbox']]", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
                let counter = 0;
                const uncheckedWords = new Array();
                for (let i = 0; i < checkBox.snapshotLength; i++) {
                    const word = interestWords.snapshotItem(i).textContent;
                    const box = checkBox.snapshotItem(i);

                    if (request.keepWords.includes(word)) {
                        if (!box.hasAttribute("checked")) box.click();
                    } else if(box.hasAttribute("checked")) {
                        box.click();
                        counter++;
                        uncheckedWords.push(word);
                    }
                }

                sendResponse({ count: counter, uncheckedwords: uncheckedWords });

                break;
            }

            case "keep": {
                const interestWords = document.evaluate("//section[@aria-labelledby='detail-header']//span[../..//input[@checked]]", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);

                getLocalStorage("keepwords").then((keepWords) => {

                    for (let i = 0; i < interestWords.snapshotLength; i++) {

                        if (!keepWords.includes(interestWords.snapshotItem(i).textContent)) {
                            keepWords.push(interestWords.snapshotItem(i).textContent);
                        }

                    }

                    chrome.storage.local.set({ keepwords: keepWords });
                    sendResponse({ count: interestWords.snapshotLength, keepwords: keepWords });

                });



                break;
            }
        }

        return true;

    });


})();