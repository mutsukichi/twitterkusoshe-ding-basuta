(async function () {

    importScripts("./common.js");

    chrome.runtime.onInstalled.addListener(async (details) => {

        const initializeItem = new Object();

        if (details.reason == "install") {

            initializeItem.checkInterval = 300000;
            initializeItem.keepwords = new Array();
            initializeItem.successed = false;

        } else if (details.reason == "update") {

            if (!Array.isArray(await getLocalStorage("keepwords"))) {
                initializeItem.keepwords = new Array();
            }


        }

        if(Object.keys(initializeItem).length > 0) {

            chrome.storage.local.set(initializeItem);

        }
    });

    let enabled = await getLocalStorage("enabled");

    chrome.alarms.get("interestCheck", (alarm) => {
        if (!alarm) interestCheck();
    })


    chrome.runtime.onMessage.addListener(async (request, sender, sendResponse) => {
        switch (request.process) {
            case "enable": {
                enabled = true;
                const lastCheck = await getLocalStorage("lastCheck");
                const checkInterval = await getLocalStorage("checkInterval");
                if (Date.now() - lastCheck >= checkInterval) interestCheck()
                sendResponse(true);
                break;
            }
            case "disable": {
                enabled = false;
                chrome.alarms.clearAll((wasCleared) => {
                    if (wasCleared) console.log("登録済みのアラームを全て削除")
                });
                sendResponse(true);
                break;
            }
            case "forceCheck": {
                interestCheck();
                sendResponse(true);
                break;
            }
            case "keep": {
                keepCheckbox();
                sendResponse(true);
                break;
            }
            case "changeSettings": {
                chrome.alarms.get("interestCheck", async (alarm) => {
                    if (alarm) {
                        const lastCheck = await getLocalStorage("lastCheck");
                        const checkInterval = await getLocalStorage("checkInterval");
                        const currentTime = Date.now();
                        if (currentTime - lastCheck >= checkInterval) {
                            interestCheck()
                        } else {
                            chrome.alarms.create("interestCheck", { when: lastCheck + parseInt(checkInterval) });
                        }
                    }
                });

                sendResponse(true);
                break;
            }

        }

        switch (request.status) {
            case "loaded": {
                loadedFunction();
                break;
            }
            case "timeout": {
                timeoutFunction();
                break;
            }
        }

        return true;
    });

    chrome.alarms.onAlarm.addListener((alarm) => {
        if (alarm.name == "interestCheck") interestCheck();
    });

    let loadedFunction = () => { };
    let timeoutFunction = () => { };

    const uncheckLock = new Lock();
    const keepLock = new Lock();


    async function interestCheck() {
        if (!enabled | uncheckLock.isLocked() | keepLock.isLocked()) return;
        uncheckLock.lock();
        const enableNotifications = await getLocalStorage("notifications");
        const checkInterval = await getLocalStorage("checkInterval");
        const keepWords = await getLocalStorage("keepwords");
        const url = "https://twitter.com/settings/your_twitter_data/twitter_interests";
        setAlarm(checkInterval);
        chrome.tabs.create({ url: url, active: false }, (tab) => {

            sendMessageToEvent({ popupStatus: "checking" });
            chrome.storage.local.set({ successed: false });

            const uncheckProcess = new Promise((resolve, reject) => {

                let retryCount = 0;

                loadedFunction = () => {
                    chrome.tabs.sendMessage(tab.id, { process: "uncheck", keepWords: keepWords }, (response) => {
                        if (enableNotifications & response.count > 0) {
                            const notificationOptions = {
                                type: "basic",
                                title: "興味関心のチェックを外しました",
                                message: "興味関心のチェックを" + response.count + "個外しました。\n解除項目：" + response.uncheckedwords,
                                iconUrl: "png/notificationicon.png"
                            };
                            chrome.notifications.create(notificationOptions);
                            saveLog(response.uncheckedwords);
                        }
                        resolve(response.count);
                    })
                };

                timeoutFunction = () => {
                    retryCount++;
                    if (retryCount >= 3) {
                        reject(new Error("リトライカウントが3回に達したため"));
                    } else {
                        chrome.tabs.reload(tab.id, { bypassCache: true });
                    }
                };


            });

            const timeout = timeoutProcess(30000);

            Promise.race([uncheckProcess, timeout]).then(async (result) => {
                sendMessageToEvent({ popupStatus: "successed" });
                chrome.storage.local.set({ successed: true });
                if (result > 0) await waitProcess(3000);
            }).catch((result) => {
                const notificationOptions = {
                    type: "basic",
                    title: "興味関心の解除に失敗しました",
                    message: "興味関心の読み込みが失敗したため、チェックの解除ができませんでした。\n原因：" + result,
                    iconUrl: "png/notificationicon.png"
                };
                chrome.notifications.create(notificationOptions);
                setAlarm(Math.min(checkInterval, 300000));
                sendMessageToEvent({ popupStatus: "failed" });
            }).finally(() => {
                uncheckLock.unlock();
                chrome.tabs.remove(tab.id);
            })



        });

    }


    async function keepCheckbox() {
        if (keepLock.isLocked()) return;
        await uncheckLock.waitUnlock();
        keepLock.lock();
        const enableNotifications = await getLocalStorage("notifications");
        const url = "https://twitter.com/settings/your_twitter_data/twitter_interests";
        chrome.tabs.create({ url: url, active: false }, (tab) => {

            const keepProcess = new Promise((resolve, reject) => {

                let retryCount = 0;

                loadedFunction = () => {
                    chrome.tabs.sendMessage(tab.id, { process: "keep" }, (response) => {

                        if (enableNotifications) {
                            let notificationOptions;

                            if (response.count > 0) {

                                notificationOptions = {
                                    type: "basic",
                                    title: "興味関心のチェック済みの項目を保存しました。",
                                    message: "興味関心のチェック済み項目" + response.count + "個を保存し、チェック解除の対象外に設定しました。\n保護項目：" + response.keepwords,
                                    iconUrl: "png/notificationicon.png"
                                };

                            } else {

                                notificationOptions = {
                                    type: "basic",
                                    title: "興味関心のチェック済み項目はありませんでした。",
                                    message: "チェック済み項目がなかったため、チェック解除の対象外の項目は設定されません。",
                                    iconUrl: "png/notificationicon.png"
                                };

                            }

                            chrome.notifications.create(notificationOptions);

                        }

                        resolve("done");
                    })
                };

                timeoutFunction = () => {
                    retryCount++;
                    if (retryCount >= 3) {
                        reject(new Error("リトライカウントが3回に達したため"));
                    } else {
                        chrome.tabs.reload(tab.id, { bypassCache: true });
                    }
                };

            });

            const timeout = timeoutProcess(30000);

            Promise.race([keepProcess, timeout]).catch((result) => {
                const notificationOptions = {
                    type: "basic",
                    title: "興味関心の取得が失敗しました",
                    message: "興味関心の読み込みが失敗したため、チェック済み項目の取得ができませんでした。\n原因：" + result,
                    iconUrl: "png/notificationicon.png"
                };
                chrome.notifications.create(notificationOptions);
            }).finally(() => {
                keepLock.unlock();
                chrome.tabs.remove(tab.id);
            })

        });

    }

    async function saveLog(uncheckedWords) {
        const checkedDateLocalString = (new Date()).toLocaleString('ja-JP');
        const uncheckLog = await (async () => {
            const saveData = await getLocalStorage("uncheckLog");
            if (Array.isArray(saveData)) {
                return saveData;
            } else {
                return new Array();
            }
        })();
        const newLog = new Array();
        for (const uncheckWord of uncheckedWords) {
            newLog.push({date: checkedDateLocalString, word: uncheckWord});
        }

        chrome.storage.local.set({uncheckLog: newLog.concat(uncheckLog).slice(0,200)});
    }


    async function setAlarm(interval = 300000) {
        const lastCheck = Date.now();
        const saveData = { lastCheck: lastCheck };
        chrome.storage.local.set(saveData);
        chrome.alarms.create("interestCheck", { when: lastCheck + parseInt(interval) });

    }

})();